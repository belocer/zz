const init = () => {
  let myMap;

  const mapClick = document.querySelector('.map__image');

  mapClick.addEventListener('click', function() {
    if (!myMap) {
      var myMap = new ymaps.Map('map', {
        center: [59.87961523802375,30.26087806878663],
        zoom: 14
      }, {
        searchControlProvider: 'yandex#search'
      });

      MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
        '<div style="color: #FFFFFF; font-weight: bold;">$[properties.iconContent]</div>'
      ),

        myPlacemark = new ymaps.Placemark(myMap.getCenter(), {
          hintContent: ''
        }, {
          iconLayout: 'default#image',
          iconImageHref: './img/union.svg',
          iconImageSize: [50, 70],
          iconImageOffset: [-30, -50]
        });

      myMap.geoObjects.add(myPlacemark);

      // myMap.behaviors.disable('scrollZoom');

      // if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
      //     myMap.behaviors.disable('drag');
      // }

    }
  });
}