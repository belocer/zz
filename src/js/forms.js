const maskPhone = (selector) => {

  try{
    let setCursorPosition = (pos, elem) => {
      elem.focus();

      if(elem.setSelectionRange) {
        elem.setSelectionRange(pos, pos);
      } else if (elem.createTextRange){
        let range = elem.createTextRange();

        range.collapse(true);
        range.moveEnd('character', pos);
        range.moveStart('character', pos);
        range.select();
      }

    };

    function createMask(event) {
      let matrix = '+7 (___) ___ __ __',
        i = 0,
        def = matrix.replace(/\D/g, ''),
        val = this.value.replace(/\D/g, '');

      if(def.length >= val.length){
        val = def;
      }

      this.value = matrix.replace(/./g, function(a){
        return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? '' : a;
      });

      if(event.type === 'blur') {
        if(this.value.length == 2) {
          this.value = '';
        }
      } else{
        setCursorPosition(this.value.length, this)
      }
    }

    let inputs = document.querySelectorAll(selector);
    inputs.forEach(input => {
      input.addEventListener('input', createMask);
      input.addEventListener('focus', createMask);
      input.addEventListener('blur', createMask);
    });
  } catch(e) {}


};

const forms = (formSelector) => {

  try{
    const form = document.querySelector(formSelector),
      inputs = form.querySelectorAll('input');

    const fileInputs = form.querySelectorAll('[name="file"]');

    const postData = async(url, data) => {
      let res = await fetch(url, {
        method: "POST",
        body: data
      });

      return await res.text();
    };

    const clearInputs = () => {
      inputs.forEach(item => {
        item.value = '';
      });

      inputs.forEach(item => {
        item.value = '';

        document.querySelector('.file__text').textContent = 'Прикрепите файл';
      });
    };


    form.addEventListener('submit', (e) => {
      e.preventDefault();

      const formData = new FormData(form);

      postData('./server.php', formData)
        .then(res => {
          console.log('Сообщение отправлено')
        })
        .catch(() =>   console.log('Сообщение не отправлено')
        )
        .finally(() => {
          clearInputs();
        });
    });
  } catch(e) {}

};


const drop = (fileId) => {

  try{
    const fileInputs = document.getElementById(fileId);

    fileInputs.addEventListener('input', () => {

      let dots;

      const arr = fileInputs.files[0].name.split('.');

      arr[0].length > 7 ? dots = '...' : dots = '.';
      const name = arr[0].substring(0, 7) + dots + arr[1];
      console.log(document.querySelector('label[for="'+ fileId +'"]'));
      document.querySelector('label[for="' + fileId + '"]>span.file__text').textContent = name;
    });
  } catch(e) {}


};
