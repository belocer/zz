<footer class="big_footer">
    <div class="footer__first-line">
        <h3 class="footer__first-line__header">Бесплатный звонок по всей России</h3>
        <div class="footer__first-line__phone">
            <div class="icon_phone"></div>
            <div class="num_phone">8 921 755 21 33</div>
        </div>
    </div>

    <div class="wrapper footer_flex_wrap">
        <div class="footer__first-column">
            <h4 class="footer__first-column__head">наши проекты</h4>

            <ul class="footer-wrap-link">
                <li class="footer-link__li"><a href="#" class="footer-link__a">Москва.Рекламанащитах.рф</a></li>
                <li class="footer-link__li"><a href="#" class="footer-link__a">Рекламавметро.рф</a></li>
                <li class="footer-link__li"><a href="#" class="footer-link__a">Рекламанатранспорте.рф</a></li>
                <li class="footer-link__li"><a href="#" class="footer-link__a">Реклама-в-лифтах.рф</a></li>
                <li class="footer-link__li"><a href="#" class="footer-link__a">Реклама-на-радио.рф</a></li>
                <li class="footer-link__li"><a href="#" class="footer-link__a">Dooh.ru</a></li>
            </ul>
        </div>

        <div class="footer__second-column">
            <h4 class="footer__second-column__head">связаться с нами</h4>
            <h5 class="footer-under-head">
                <span class="footer-under-head__icon"></span>
                <span class="footer-under-head__text">Бесплатный звонок по России</span>
            </h5>

            <ul class="footer-wrap-second-column__phone">
                <li class="footer-second-column__li"><a href="#" class="footer-second-column__a">8 (812) 702 05 07</a>
                </li>
                <li class="footer-second-column__li"><a href="#" class="footer-second-column__a">8 (800) 770 08 52</a>
                </li>
                <li class="footer-second-column__li"><a href="#" class="footer-second-column__a">8 (964) 342 22 02</a>
                </li>
                <li class="footer-second-column__li"><a href="#" class="footer-second-column__a">8 (964) 342 22 02</a>
                </li>
                <li class="footer-second-column__li"><a href="#" class="footer-second-column__a">8 (499) 393-00-00</a>
                </li>
            </ul>

            <h5 class="footer-under-head">
                <span class="footer-under-head__iconEmail"></span>
                <span class="footer__email">info@dooh.ru</span>
            </h5>

            <h5 class="footer-under-head">
                <span class="footer-under-head__iconRupor"></span>
                <span class="footer-under-head__text">По вопросам PR и сотрудничества</span>
                <span class="footer__email">info@dooh.ru</span>
            </h5>
        </div>

        <div class="footer__third-column">
            <h5 class="footer-under-head">
                <span class="iconListPlus"></span>
                <span class="footer-under-head__text">Пригласить в тендер</span>
                <span class="footer__email">info@dooh.ru</span>
            </h5>
            <h5 class="footer-under-head">
                <span class="iconUserPlus"></span>
                <span class="footer-under-head__text">Стать поставщиком</span>
                <span class="footer__email">info@dooh.ru</span>
            </h5>
            <h5 class="footer-under-head">
                <span class="iconManyUsers"></span>
                <span class="footer-under-head__text">Работа в нашей команде</span>
                <span class="footer__email">info@dooh.ru</span>
            </h5>
            <h5 class="footer-under-head">
                <span class="iconHands"></span>
                <span class="footer-under-head__text">Партнёрская программа</span>
                <span class="footer__email">info@dooh.ru</span>
            </h5>
        </div>

        <div class="footer__fourth-column">
            <ul class="four-icon-social">
                <li class="four-icon-social__li"><a href="#" class="tel"></a></li>
                <li class="four-icon-social__li"><a href="#" class="we"></a></li>
                <li class="four-icon-social__li"><a href="#" class="whats"></a></li>
                <li class="four-icon-social__li"><a href="#" class="inst"></a></li>
            </ul>

            <a href="#" class="footer__up">
                <span class="wrapper_footer__upCircle">
                    <span class="footer__upCircle">
                        <span class="footer__upIcon"></span>
                    </span>
                </span>

                <span class="our-project__watchAllText">Наверх</span>
            </a>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="wrapper align_items_center">
            <div class="footer-bottom__left">
                <p class="footer-bottom__text">Все права зарегистрированы. Данный сайт является информационным, и не
                    является публичной офертой,
                    определяемой положениями ст. 437(2) ГК РФ Служба клиентского сервиса</p>
                <a href="#" class="footer-bottom__link">Политика обработки данных</a> / <a href="#"
                                                                                           class="footer-bottom__link">Политика
                    конфиденциальности</a>
            </div>
            <div class="footer-bottom__right">
                <div class="footer-bottom__right__icon"></div>
                <div class="footer-bottom__right__text">
                    <span class="footer-bottom__right_text_first_line">Сделано в</span>
                    <span class="footer-bottom__right_text_second_line">WEB.MEDIA </span>
                </div>
            </div>
        </div>
    </div>
</footer>

<script src="js/main.min.js?ver=<?= rand(10000, 99999) ?>"></script>
</body>
</html>