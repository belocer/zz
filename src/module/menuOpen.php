<div class="menuOpen__fon"></div>
    <div class="menuOpen">
        <div class="menuOpen__wrapper">
            <div class="menu__list">
                <div class="menu__horizontal">
                    <ul class="menu__horizontalList">
                        <li class="menu__horizontalItem"><a href="#">Адресная программа</a> </li>
                        <li class="menu__horizontalItem">
                            <a href="#">Об агенстве</a>
                        </li>
                        <li class="menu__horizontalItem">
                            <a href="project.php">Кейсы</a>
                        </li>
                        <li class="menu__horizontalItem">
                            <a href="#">Партнерам</a>
                        </li>
                        <li class="menu__horizontalItem">
                            <a href="contact.php">Контакты</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="menu__vertical">
            <ul class="menu__verticalList">
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_1">Наружная реклама</a>
                    </div>
                    </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_2">на транспорте</a>
                    </div>
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_3">в метро</a>
                    </div>
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_11">сувенирЬI</a>
                    </div>
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_9">в интернете</a>
                    </div>
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_4">ПромоутерьI</a>
                    </div>
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_10">В лифтах</a>
                    </div>
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_5">в прессе</a>
                    </div>
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_7">на тв</a>
                    </div>
                    
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                        <a href="project.php#tab_8">по радио</a>
                    </div>  
                    
                </li>
                <li class="menu__verticalItem">
                    <div class="menuOpenLi__wrapper">
                    <a href="project.php#tab_6">indoor</a>
                    </div>  
                    
                </li>
            </ul>
        </div>
        <div class="menuOpen__image">
            <div class="menuOpen__imageAll menuOpen__image1">
                <img src="img/bilbord.svg" alt="Наружная реклама">
            </div>
            <div class="menuOpen__imageAll menuOpen__image2">
                <img src="img/bus.png" alt="на транспорте">
            </div>
            <div class="menuOpen__imageAll menuOpen__image3">
                <img src="img/metro.png" alt="в метро">
            </div>
            <div class="menuOpen__imageAll menuOpen__image4">
                <img src="img/suvenir.png" alt="сувенирЬI">
            </div>
            <div class="menuOpen__imageAll menuOpen__image5">
                <img src="img/pic.png" alt="в интернете">
            </div>
            <div class="menuOpen__imageAll menuOpen__image6">
                <img src="img/banan.png" alt="ПромоутерьI">
            </div>
            <div class="menuOpen__imageAll menuOpen__image7">
                <img src="img/lift.png" alt="В лифтах">
            </div>
            <div class="menuOpen__imageAll menuOpen__image8">
                <img src="img/dog.png" alt="в прессе">
            </div>
            <div class="menuOpen__imageAll menuOpen__image9">
                <img src="img/tv.png" alt="на тв">
            </div>
            <div class="menuOpen__imageAll menuOpen__image10">
                <img src="img/radio.png" alt="по радио">
            </div>
            <div class="menuOpen__imageAll menuOpen__image11">
                <img src="img/indoor.png" alt="indoor">
            </div>
        </div>

        <div class="lang-wrapper">
            <a href="#">En</a>
            <a href="#" class="active">Ru</a>
        </div>

    </div>