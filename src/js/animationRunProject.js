const animationRunProject = () => {


  try{
    const scroll = calcScroll();
    const start = document.querySelector('.run_project');
    const close = document.querySelector('.close__modalStartProject');

    start.addEventListener('click', function(e) {

      e.preventDefault();

      document.body.style.marginRight = `${scroll}px`;

      document.querySelector('.block-animation').classList.add('block-animationAfter');
      document.body.style.overflowY = "hidden";
      document.body.style.height = "100%";

      function showModal() {
        document.querySelector('.modal__startProject').classList.add('showModal');
        document.querySelector('.modal__startProject-logo').classList.add('showModal');
      }
      setTimeout(showModal, 400);

      function showModalEye(eyeSelector) {
        document.querySelector(eyeSelector).classList.add('showModal');
      }
      setTimeout(showModalEye, 1500, '.modal__eyeItem1');
      setTimeout(showModalEye, 1200, '.modal__eyeItem2');
      setTimeout(showModalEye, 1600, '.modal__eyeItem3');
      setTimeout(showModalEye, 1400, '.modal__eyeItem4');
      setTimeout(showModalEye, 1300, '.modal__eyeItem5');
      setTimeout(showModalEye, 1700, '.modal__eyeItem6');

      /* Денис - Скрываю "создать проект" на главной */
      const wrap_menu = document.querySelector('.wrap_menu')
      if(wrap_menu) wrap_menu.style.zIndex = '9'

    })

    close.addEventListener('click', function(e) {

      e.preventDefault();

      document.body.style.overflow = "";
      document.body.style.marginRight = `0px`;

      document.querySelector('.modal__startProject').classList.remove('showModal');

      document.querySelector('.modal__startProject-logo').classList.remove('showModal');

      function hideModal() {
        document.querySelector('.block-animation').classList.remove('block-animationAfter');
        document.body.style.overflowY = "auto";
      }

      setTimeout(hideModal, 400);

      function showModalEye(eyeSelector) {
        document.querySelector(eyeSelector).classList.remove('showModal');
      }
      setTimeout(showModalEye, 50, '.modal__eyeItem1');
      setTimeout(showModalEye, 300, '.modal__eyeItem2');
      setTimeout(showModalEye, 100, '.modal__eyeItem3');
      setTimeout(showModalEye, 150, '.modal__eyeItem4');
      setTimeout(showModalEye, 200, '.modal__eyeItem5');
      setTimeout(showModalEye, 250, '.modal__eyeItem6');

      /* Денис - Скрываю "создать проект" на главной */
      const wrap_menu = document.querySelector('.wrap_menu')
      if(wrap_menu) wrap_menu.style.zIndex = ''
      const wrapp_eleven = document.querySelector('.wrapp_eleven')
      if(wrapp_eleven) wrapp_eleven.style.zIndex = ''
      setTimeout(() => {
        const body_tag = document.querySelector('body')
        console.log(body_tag.getAttribute('style'));
        if(body_tag.getAttribute('style') == 'overflow: hidden; margin-right: 17px; height: 100%;' ||
          body_tag.getAttribute('style') == 'margin-right: 0px; height: 100%; overflow-y: auto;' ||
          body_tag.getAttribute('style') == 'margin-right: 0px; height: 100%;'
        ) {
          body_tag.removeAttribute('style')
          body_tag.setAttribute('style', 'overflow: hidden;')
        }
      }, 700)


    })
  } catch(e) {}


  function calcScroll() {
    let div = document.createElement('div');

    div.width = '50px';
    div.height = '50px';
    div.style.overflowY = 'scroll';
    div.style.visibility = 'hidden';


    document.body.appendChild(div);
    let scrollWidth = div.offsetWidth - div.clientWidth;
    div.remove();

    return scrollWidth;
  }
}

animationRunProject();