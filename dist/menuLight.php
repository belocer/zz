<div class="menu-wrapper">
    <div class="wrap_first_line">
        <div class="menu__main">
            <div class="logo__wrap">
                <a href="main.php" class="logo__link menu__logo menu__logoBlack"></a>
            </div>
            <div class="menu__right">
                <div class="nav-link__head">
                    <a href="#" class="run_project color_red">Запустить проект</a>
                    <button class="menuOpen__hamb">
                        <span>toggle menu</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>