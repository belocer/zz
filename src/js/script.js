window.addEventListener('load', () => {

    tabs('tabs', 'tab__item', 'tabContent');
    tabs('projectTabs', 'projectTabs__item', 'projectTabsContent');

    try{
        ymaps.ready(init);
    } catch(e) {}
    
    maskPhone('[id="phone"]');
    maskPhone('[id="phone2"]');
    maskPhone('[id="phone3"]');
    maskPhone('[id="phone4"]');

    forms('#form__1');
    drop('file__1');
    drop('file__2');
    drop('file__3');
    drop('file__4');
    drop('file__5');

});