window.addEventListener('load', () => {

  /* Анимация бегущей строки */
  let ticker_violet = document.querySelector('.ticker_violet')

  if (ticker_violet) {
    // Общая ширина строки
    let width_ticker = ticker_violet.children.length * ticker_violet.children[0].offsetWidth

    let count = 0
    setInterval(() => {
      ticker_violet.style.transform = 'translateX(-' + count + 'px)'
      count = count + 8
      // Если строка закончилась, то сбрасываю сдвиг влево
      if (count > (width_ticker - document.body.clientWidth)) {
        count = 0
        ticker_violet.style.transform = 'translateX(-' + count + 'px)'
      }
    }, 45)
  }
})