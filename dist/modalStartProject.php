<div class="block-animation"></div>
    <div class="menu-wrapper">
        <div class="modal__startProject-logo">
            <div class="menu__main">
                <div class="logo__wrap">
                    <a href="main.php" class="logo__link menu__logo"></a>
                </div>
            </div>
        </div>
    </div>
 
    <div class="modal__startProject">
        <div class="modal__eye">
            <div class="modal__eyeItem modal__eyeItem1"></div>
            <div class="modal__eyeItem modal__eyeItem2"></div>
            <div class="modal__eyeItem modal__eyeItem3"></div>
            <div class="modal__eyeItem modal__eyeItem4"></div>
            <div class="modal__eyeItem modal__eyeItem5"></div>
            <div class="modal__eyeItem modal__eyeItem6"></div>
        </div>
        <div class="fmModal__wrapper">
            <h2>Запустить проект</h2>
        </div>
        
        <img src="img/closeModal.svg" alt="close" class="close__modalStartProject">
        <form action="#" class="form" method="post" id="form__1">
            <div class="fm__wrapper form__wrapper">
                <div class="form__all">
                    <div class="form__input form__input-2">    
                        <input type="text"
                            id="name__5" 
                            name="user_name"
                            autocomplete="off"
                            oninvalid="this.setCustomValidity(' ')"
                            oninput="setCustomValidity('')"
                            placeholder="Ваше имя"
                        > 
                    </div>
                    <label class="placeinput form__input form__input-2" id="form__contact1" >
                        <input 
                            required 
                            autocomplete="off"
                            type="text" 
                            id="form__contact" 
                            name="user_contact"
                            oninvalid="this.setCustomValidity(' ')" oninput="setCustomValidity('')" 
                         />
                        <div class="place_holder">
                        Контактный телефон или e-mail  
                        <span> &nbsp;*</span></div>
                    </label>
                </div>
                <div class="form__all">
                    <div class="form__input">    
                        <input type="text"
                            id="task" 
                            name="user_task"
                            autocomplete="off"
                            oninvalid="this.setCustomValidity(' ')"
                            oninput="setCustomValidity('')"
                            placeholder="Опишите задачу"
                        > 
                    </div>
                </div>
                <div class="file">
                    <input type="file" name="file"  class="file__input" id="file__5"> 
                    <label for="file__5" class="file__label" >
                        <img class="file__img" src="img/file.svg" alt="Прикрепите файл">
                    <span class="file__text">Прикрепите файл</span>
                    </label>
                </div>
                <div class="send">
                    <button class="form__btn" type="submit">
                    Отправить заявку 
                    <img src="img/send.svg" alt="отправить">
                    </button>
                    <p class="form__send">
                        Нажимая на кнопку “Отправить заявку”, вы даете согласие на 
                        <a href="#" class="form__sendPolitics">обработку персональных данных</a> и соглашаетесь с 
                        <a href="#" class="form__sendPolitics">политикой конфиденциальности</a> 
                    </p>
                </div>
            </div>
        </form>
    </div>