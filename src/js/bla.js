window.addEventListener('load', () => {

  /* BLA BLA BLA */
  let opacity_ = document.querySelectorAll('.opacity')
  if (opacity_ && opacity_.length > 0) {
    window.addEventListener('scroll', anim_bla)
    function anim_bla (e) {

      let horse = document.querySelector('.horse')
      if (window.pageYOffset > 1200 && window.pageYOffset < 2000 && horse) {

        horse.style.opacity = '0'

        setTimeout(() => {
          opacity_[0].style.transition = 'opacity .3s'
          opacity_[0].style.opacity = '1'
        }, 100)

        setTimeout(() => {
          opacity_[1].style.transition = 'opacity .3s'
          opacity_[1].style.opacity = '1'
        }, 250)

        setTimeout(() => {
          opacity_[2].style.transition = 'opacity .3s'
          opacity_[2].style.opacity = '1'
        }, 400)

      } else {

        if (horse) {
          horse.style.opacity = '1'
        }

        setTimeout(() => {
          opacity_[0].style.transition = 'opacity .3s'
          opacity_[0].style.opacity = '0'
        }, 100)

        setTimeout(() => {
          opacity_[1].style.transition = 'opacity .3s'
          opacity_[1].style.opacity = '0'
        }, 250)

        setTimeout(() => {
          opacity_[2].style.transition = 'opacity .3s'
          opacity_[2].style.opacity = '0'
        }, 400)
      }

      if (window.pageYOffset > 1300) {

        setTimeout(() => {
          opacity_[3].style.transition = 'opacity .3s'
          opacity_[3].style.opacity = '1'
        }, 550)

        setTimeout(() => {
          opacity_[4].style.transition = 'opacity .3s'
          opacity_[4].style.opacity = '1'
        }, 700)

        setTimeout(() => {
          opacity_[5].style.transition = 'opacity .3s'
          opacity_[5].style.opacity = '1'
        }, 850)

        setTimeout(() => {
          opacity_[6].style.transition = 'opacity .3s'
          opacity_[6].style.opacity = '1'
        }, 1000)
      } else {
        setTimeout(() => {
          opacity_[3].style.transition = 'opacity .3s'
          opacity_[3].style.opacity = '0'
        }, 550)

        setTimeout(() => {
          opacity_[4].style.transition = 'opacity .3s'
          opacity_[4].style.opacity = '0'
        }, 700)

        setTimeout(() => {
          opacity_[5].style.transition = 'opacity .3s'
          opacity_[5].style.opacity = '0'
        }, 850)

        setTimeout(() => {
          opacity_[6].style.transition = 'opacity .3s'
          opacity_[6].style.opacity = '0'
        }, 1000)
      }

      if (window.pageYOffset > 1550) {

        setTimeout(() => {
          opacity_[7].style.transition = 'opacity .3s'
          opacity_[7].style.opacity = '1'
        }, 1150)

        setTimeout(() => {
          opacity_[8].style.transition = 'opacity .3s'
          opacity_[8].style.opacity = '1'
        }, 1300)

        setTimeout(() => {
          opacity_[9].style.transition = 'opacity .3s'
          opacity_[9].style.opacity = '1'
        }, 1450)

        setTimeout(() => {
          opacity_[10].style.transition = 'opacity .3s'
          opacity_[10].style.opacity = '1'
        }, 1600)
      } else {
        setTimeout(() => {
          opacity_[7].style.transition = 'opacity .3s'
          opacity_[7].style.opacity = '0'
        }, 1150)

        setTimeout(() => {
          opacity_[8].style.transition = 'opacity .3s'
          opacity_[8].style.opacity = '0'
        }, 1300)

        setTimeout(() => {
          opacity_[9].style.transition = 'opacity .3s'
          opacity_[9].style.opacity = '0'
        }, 1450)

        setTimeout(() => {
          opacity_[10].style.transition = 'opacity .3s'
          opacity_[10].style.opacity = '0'
        }, 1600)
      }

      /* Оранжевые глаза справа */
      if (window.pageYOffset > 1500) {
        let bla__img = document.querySelectorAll('.bla__img')
        setTimeout(() => {
          bla__img[0].style.transition = 'all .3s'
          bla__img[0].style.opacity = '1'
          bla__img[0].style.transform = 'translateX(0)'
        }, 800)

        setTimeout(() => {
          bla__img[1].style.transition = 'all .3s'
          bla__img[1].style.opacity = '1'
          bla__img[1].style.transform = 'translateX(0)'
        }, 950)

        setTimeout(() => {
          bla__img[2].style.transition = 'all .3s'
          bla__img[2].style.opacity = '1'
          bla__img[2].style.transform = 'translateX(0)'
        }, 1100)

        setTimeout(() => {
          bla__img[3].style.transition = 'all .3s'
          bla__img[3].style.opacity = '1'
          bla__img[3].style.transform = 'translateX(0)'
        }, 1250)

      } else {

        let bla__img = document.querySelectorAll('.bla__img')
        setTimeout(() => {
          bla__img[0].style.transition = 'all .3s'
          bla__img[0].style.opacity = ''
          bla__img[0].style.transform = ''
        }, 800)

        setTimeout(() => {
          bla__img[1].style.transition = 'all .3s'
          bla__img[1].style.opacity = ''
          bla__img[1].style.transform = ''
        }, 950)

        setTimeout(() => {
          bla__img[2].style.transition = 'all .3s'
          bla__img[2].style.opacity = ''
          bla__img[2].style.transform = ''
        }, 1100)

        setTimeout(() => {
          bla__img[3].style.transition = 'all .3s'
          bla__img[3].style.opacity = ''
          bla__img[3].style.transform = ''
        }, 1250)
      }
    }
  }

  /* Подскролливание */
  let lastScrollTop = 0
  window.addEventListener('scroll', onScrollBla)

  function onScrollBla (e) {
    let top = window.pageYOffset
    if (lastScrollTop > top) { // Скролл вверх

      //console.log('скролл вверх');

      if (window.pageYOffset < 2100) {
        let wrap_first_line = document.querySelector('.wrap_first_line')
        wrap_first_line.style.transform = 'translateY(0px)'
      }

    } else if (lastScrollTop < top && window.pageYOffset < 1500 && window.pageYOffset > 1090) {

      //console.log('скролл вниз');
      //let bla = document.querySelector('.bla');
      //bla.scrollIntoView();
      /*
       let html = document.querySelector('html')
       html.style.transition = 'all 0s'
       setTimeout(() => {
       html.scrollTop += 200
       }, 500)*/
    }
    lastScrollTop = top
  }

})