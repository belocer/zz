window.onhashchange = function() {

  try{

    if (window.location.hash.length > 0) {
      document.querySelector(window.location.hash).click();
      if (document.querySelector(window.location.hash).className == tabSelector) {
        for (let i = 0; i < tab.length; i++) {
          if (document.querySelector(window.location.hash) == tab[i]) {

            showTabsContent(i);
            window.scrollTo(pageXOffset, 0);

            break;
          }
        }
      }
    }

  } catch(e) {}
}

const tabs = (tabsSelector, tabSelector, tabContentSelector) => {

  try{

    const tab = document.getElementsByClassName(tabSelector);
    const tabContent = document.getElementsByClassName(tabContentSelector);
    const tabs =  document.getElementById(tabsSelector);

    hideTabsContent(1);

    try{
      if (window.location.hash.length > 0) {
        document.querySelector(window.location.hash).click();
        if (document.querySelector(window.location.hash).className == tabSelector) {
          for (let i = 0; i < tab.length; i++) {
            if (document.querySelector(window.location.hash) == tab[i]) {

              showTabsContent(i);
              window.scrollTo(pageXOffset, 0);
              break;
            }
          }
        }
      }

    } catch(e) {}

    tabs.onclick = function (event) {

      let target = event.target;
      if (target.className == tabSelector) {
        for (let i = 0; i < tab.length; i++) {
          if (target == tab[i]) {
            showTabsContent(i);
            break;

          }
        }
      }
    }

    function hideTabsContent(a) {
      for (var i = a; i<tabContent.length; i++) {

        tabContent[i].classList.remove('show');
        tabContent[i].classList.add("hide");
        tab[i].classList.remove('whiteborder');


      }
    }

    function showTabsContent(b){
      if (tabContent[b].classList.contains('hide')) {
        hideTabsContent(0);
        tab[b].classList.add('whiteborder');
        tabContent[b].classList.remove('hide');
        tabContent[b].classList.add('show');

        if (window.location.hash.length > 0) {
          window.scrollTo(pageXOffset, 0);
        }
      }
    }
  } catch(e) {}


}
