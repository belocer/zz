const menuOpen = () => {

  const scroll = calcScroll()
  const menuItemActive = document.querySelectorAll('.menu__verticalItem')
  const menuHorizontalList = document.querySelectorAll('.menu__horizontalItem a')
  const menuFon = document.querySelector('.menuOpen__fon')
  const menuBurger = document.querySelector('.menuOpen__hamb')

  try {
    menuBurger.addEventListener('click', function (e) {

      e.preventDefault()

      if (!this.classList.contains('is-active')) {
        this.classList.add('is-active')

        menuFon.classList.add('menuOpenAfter', 'menuOpenAfterFon')
        document.querySelector('.menuOpen').classList.add('menuOpenAfter')
        menuBurger.disabled = true

        function showMenuHorizontal () {
          document.querySelector('.full-wrapper').style.maxHeight = '0px'
          document.querySelector('.full-wrapper').style.overflow = 'hidden'
          document.querySelector('.menu__horizontal').classList.add('showMenu')
          document.querySelector('.menu__vertical').classList.add('showMenu')
          document.querySelector('.lang-wrapper').classList.add('showMenu')

          if (document.documentElement.clientWidth > 991) {
            document.querySelector('.menu__main').style.position = 'fixed'
          }
        }

        setTimeout(showMenuHorizontal, 500)

      } else {
        menuBurger.disabled = true
        this.classList.remove('is-active')

        document.querySelector('.menu__horizontal').classList.remove('showMenu')
        document.querySelector('.menu__vertical').classList.remove('showMenu')
        document.querySelector('.menu__main').style.position = 'relative'
        document.querySelector('.lang-wrapper').classList.remove('showMenu')

        function hideMenuHorizontal () {
          document.querySelector('.full-wrapper').style.maxHeight = 'auto'
          document.querySelector('.full-wrapper').style.overflow = 'visible'
          document.querySelector('.menuOpen__fon').classList.remove('menuOpenAfter')
          document.querySelector('.menuOpen').classList.remove('menuOpenAfter')
        }

        setTimeout(hideMenuHorizontal, 800)
      }
      function disabled () {
        menuBurger.disabled = false
      }

      setTimeout(disabled, 1000)

      /* Денис - Скрываю "создать проект" на главной */
      const wrap_menu = document.querySelector('.wrap_menu')
      if(wrap_menu) wrap_menu.style.zIndex = '9'
    })

  } catch (e) {}

  menuItemActive.forEach((item, i) => {

    item.addEventListener('mouseover', function () {

      menuHorizontalList.forEach(item => {
        item.style.color = '#fff'
      })

      document.querySelectorAll('.menu__verticalItem a').forEach(itemA => {
        itemA.style.webkitTextStroke = '2px #fff'
        itemA.style.color = '#fff'
        itemA.style.webkitTextFillColor = 'RGBA(255, 255, 255, 0)'
      })

      document.querySelector('.menuOpen__hamb').classList.add('menuOpen__hambWhite')
      document.querySelector('.lang-wrapper a').style.color = '#fff'
      document.querySelector('.lang-wrapper .active').style.border = '1px solid #fff'
      document.querySelector('.lang-wrapper .active').style.color = '#fff'

      item.querySelector('a').style.textStroke = '2px white'
      item.querySelector('a').style.webkitTextStroke = '2px white'
      item.querySelector('a').style.webkitTextFillColor = 'RGBA(255, 255, 255, 1)'
      item.querySelector('a').style.color = 'white'

      document.querySelectorAll('.menuOpen__imageAll').forEach(item => {
        item.style.display = 'none'
      })

      document.querySelector('.menu__logo').classList.remove('menu__logoBlack')
      document.querySelector('.run_project').classList.add('color_white')
      document.querySelector('.run_project').classList.remove('color_red')

      switch (i) {
        case 0:
          menuFon.style.background = 'radial-gradient(48.05% 122.94% at 75.65% 54.8%, rgba(255, 79, 106, 0.85) 0%, rgba(246, 23, 63, 0) 85.01%), #F6173F'
          document.querySelector('.menuOpen__image1').style.display = 'flex'
          break
        case 1:
          menuFon.style.background = 'radial-gradient(34.74% 69.45% at 69.39% 55.35%, #26FA7B 0%, rgba(25, 214, 101, 0) 100%), #13D560'
          document.querySelector('.menuOpen__image2').style.display = 'flex'
          break
        case 2:
          menuFon.style.background = 'radial-gradient(56.48% 120.26% at 52.73% 67.77%, #6486FF 0%, rgba(169, 60, 255, 0) 100%), #6E49FF'
          document.querySelector('.menuOpen__image3').style.display = 'flex'
          break
        case 3:
          menuFon.style.background = 'radial-gradient(52.58% 75.24% at 57.63% 62.8%, rgba(180, 242, 78, 0.8) 0%, rgba(152, 217, 88, 0) 100%), #89D042'
          document.querySelector('.menuOpen__image4').style.display = 'flex'
          break
        case 4:
          menuFon.style.background = 'radial-gradient(54.6% 97.88% at 47.1% 17.93%, rgba(69, 88, 252, 0.8) 0%, rgba(24, 45, 234, 0) 100%), #182DEA'
          document.querySelector('.menuOpen__image5').style.display = 'flex'
          break
        case 5:
          menuFon.style.background = 'radial-gradient(30.69% 96.65% at 64.9% 84.22%, #FFD645 0%, rgba(246, 219, 113, 0) 100%), #FFC122'
          document.querySelector('.menuOpen__image6').style.display = 'flex'
          break
        case 6:
          menuFon.style.background = 'radial-gradient(58.54% 120.93% at 57.56% 61.44%, #FF8652 0%, rgba(255, 118, 41, 0) 100%), #FF6F1E'
          document.querySelector('.menuOpen__image7').style.display = 'flex'
          break
        case 7:
          menuFon.style.background = 'radial-gradient(34.56% 92.65% at 58.46% 52.1%, rgba(255, 145, 198, 0.696) 0%, rgba(244, 51, 144, 0) 100%), #F54584'
          document.querySelector('.menuOpen__image8').style.display = 'flex'
          break
        case 8:
          menuFon.style.background = 'radial-gradient(77.64% 77.64% at 50% 54.56%, #3DADFF 0%, rgba(26, 147, 235, 0) 100%), #4BACF3'
          document.querySelector('.menuOpen__image9').style.display = 'flex'
          break
        case 9:
          menuFon.style.background = 'radial-gradient(43.51% 75.03% at 53.36% 52%, rgba(94, 255, 252, 0.68) 0%, rgba(33, 223, 220, 0) 100%), #16CCC9'
          document.querySelector('.menuOpen__image10').style.display = 'flex'
          break
        case 10:
          menuFon.style.background = 'radial-gradient(62.78% 87.81% at 50% 50%, #AD33F8 0%, rgba(141, 25, 212, 0) 100%), #8D19D5'
          document.querySelector('.menuOpen__image11').style.display = 'flex'
          break

        default:
          break
      }

    })

    item.addEventListener('mouseout', function () {

      menuHorizontalList.forEach(item => {
        item.style.color = '#282937'
      })

      document.querySelectorAll('.menu__verticalItem a').forEach(itemA => {
        itemA.style.webkitTextStroke = '2px #282937'
        itemA.style.color = '#282937'
        itemA.style.webkitTextFillColor = 'RGBA(255, 255, 255, 1)'
      })

      document.querySelector('.menuOpen__hamb').classList.remove('menuOpen__hambWhite')
      document.querySelector('.lang-wrapper a').style.color = '#282937'
      document.querySelector('.lang-wrapper .active').style.border = '1px solid #282937'
      document.querySelector('.lang-wrapper .active').style.color = '#282937'

      menuFon.style.background = '#F7F7F7'

      document.querySelectorAll('.menuOpen__imageAll').forEach(item => {
        item.style.display = 'none'
      })
      document.querySelector('.menu__logo').classList.add('menu__logoBlack')
      document.querySelector('.run_project').classList.remove('color_white')
      document.querySelector('.run_project').classList.add('color_red')

    })

    item.addEventListener('click', function (e) {

      item.querySelector('a').click()
      menuBurger.click()
    })

  })

  function calcScroll () {

    let div = document.createElement('div')
    div.width = '50px'
    div.height = '50px'
    div.style.overflowY = 'scroll'
    div.style.visibility = 'hidden'
    document.body.appendChild(div)
    let scrollWidth = div.offsetWidth - div.clientWidth
    div.remove()

    return scrollWidth
  }
}

menuOpen()



 

 
