window.addEventListener('load', () => {
  const menu_wrapper = document.querySelector('.menu-wrapper')
  const select_lang = document.querySelector('.select_lang')
  const run_project = document.querySelector('.run_project')
  const wrap_logo_center = document.querySelector('.wrap_logo_center')
  const logo_center = document.querySelector('.logo_center')
  const logo_center__text = document.querySelector('.logo_center__text')
  const z1 = document.querySelector('.z1')
  const z2 = document.querySelector('.z2')
  const wrap_eye = document.querySelector('.wrap_eye')
  const wrap_first_line = document.querySelector('.wrap_first_line')

  setTimeout(() => {
    back_anim()
  }, 500)

  window.addEventListener('scroll', anim_logo)
  function anim_logo (e = null) { // Увеличиваем красный бэкграунд на первом экране

    //console.log('anim_logo');

    if (window.pageYOffset > 200 && wrap_logo_center && logo_center) {
      let scr_w = document.documentElement.clientWidth
      let scr_h = document.documentElement.clientHeight

      show_white_menu()

      wrap_logo_center.style.width = scr_w + 'px'
      wrap_logo_center.style.height = scr_h + 150 + 'px'
      wrap_logo_center.style.margin = '-248px 0 0 0'
      wrap_logo_center.style.padding = 0
      wrap_logo_center.style.transition = 'all .7s'
      wrap_logo_center.style.left = '0'

      logo_center.style.margin = '0'
      logo_center.style.padding = '0'
      logo_center.style.transition = 'all .7s'

      if(wrap_eye) {
        wrap_eye.style.opacity = '0 !important'
        wrap_eye.setAttribute('style', 'opacity: 0 !important')
      }

      let white_dashed_circleUp = document.querySelector('.white_dashed_circle')
      if(white_dashed_circleUp) {
        white_dashed_circleUp.style.opacity = '0 !important'
        white_dashed_circleUp.setAttribute('style', 'opacity: 0 !important')
      }

      let eye = document.getElementById('eye')
      if(eye) {
        eye.style.opacity = '0'
      }

      if (z1 && z2) {
        z1.style.transition = 'margin .5s'
        z2.style.transition = 'margin .5s'
        z1.style.marginTop = '-100px'
        z2.style.marginBottom = '-100px'
      }

      if (window.pageYOffset > 1000 && select_lang) {
        wrap_first_line.removeAttribute('id')
        wrap_first_line.setAttribute('id', 'menu_fixed_second')
        select_lang.style.display = ''
        run_project.style.color = ''
      }
      if (window.pageYOffset > 2100 && window.pageYOffset < 4000) {
        wrap_first_line.style.transform = 'translateY(-300px)'
      }

    } else { // Уменьшаем

      show_regular_menu()

      //console.log('Уменьшаем');

      let scr_w = document.documentElement.clientWidth
      let scr_h = document.documentElement.clientHeight

      if (wrap_logo_center && logo_center) {
        wrap_logo_center.style.width = '400px'
        wrap_logo_center.style.height = '400px'
        wrap_logo_center.style.margin = '5% auto 0'
        wrap_logo_center.style.padding = ''
        wrap_logo_center.style.transition = 'all 1s'
        wrap_logo_center.style.left = 'calc(50% - 200px)'
        wrap_logo_center.style.top = 'calc(50% - 450px)'

        logo_center.style.width = ''
        logo_center.style.height = ''
        logo_center.style.padding = ''
        logo_center.style.transition = 'all 1s'
      }

      if(wrap_eye) {
        wrap_eye.removeAttribute('style')
        wrap_eye.style.animation = 'max_min .5s ease-in-out 1s forwards'
      }

      let white_dashed_circle = document.querySelector('.white_dashed_circle')

      if(white_dashed_circle) {
        white_dashed_circle.removeAttribute('style')
        white_dashed_circle.style.animation = '8s linear 0s infinite normal none running rotate_dashed, 0.5s linear 1s 1 normal none running max_min'
        setTimeout(() => {
          white_dashed_circle.style.visibility = 'visible'
        }, 1000)
      }

      setTimeout(() => {
        let eye = document.getElementById('eye')

        if(eye) {
          eye.style.transition = 'opacity .5s'
          eye.style.opacity = '1'
          eye.style.width = '60px'
          eye.style.height = '30px'
        }
      }, 1500)

      if (z1 && z2) {
        z1.style.transition = 'margin .5s'
        z2.style.transition = 'margin .5s'
        z1.style.marginTop = ''
        z2.style.marginBottom = ''
      }

      if (window.pageYOffset < 500 && select_lang) {
        wrap_first_line.removeAttribute('id')
        select_lang.style.display = ''
        run_project.style.color = ''
      }
    }

    if(window.pageYOffset > 990) {
      menu_wrapper.setAttribute('style', 'position: fixed; top: 0px; left: 0px; right: 0px; z-index: 8' )
      const logo__link = menu_wrapper.querySelector('.logo__link')
      logo__link.classList.add('menu__logoBlack')
      const run_project = menu_wrapper.querySelector('.run_project')
      run_project.classList.remove('color_white')
      run_project.classList.add('color_red')
      const menuOpen__hamb = document.querySelector('.menuOpen__hamb')
      menuOpen__hamb.innerHTML = '<span>toggle menu</span>'
      menuOpen__hamb.removeAttribute('id')
    }
  }

  function back_anim () {
    //console.log('back_anim');

    let scr_w = document.documentElement.clientWidth
    let scr_h = document.documentElement.clientHeight

    if (wrap_logo_center && logo_center) {
      wrap_logo_center.style.padding = 0
      wrap_logo_center.style.transition = 'all 1s'
      wrap_logo_center.style.left = '0'
      logo_center.style.padding = 0
      logo_center.style.transition = 'all 1s'
    }

    let wrap_eyeUP = document.querySelector('.wrap_eye')
    if (wrap_eyeUP) {
      wrap_eyeUP.style.opacity = '0 !important'
      wrap_eyeUP.setAttribute('style', 'opacity: 0 !important')

      setTimeout(() => {
        wrap_eyeUP.style.opacity = '0 !important'
      }, 1)
    }

    let white_dashed_circleUp = document.querySelector('.white_dashed_circle')

    if (white_dashed_circleUp) {
      white_dashed_circleUp.style.opacity = '0 !important'
      white_dashed_circleUp.setAttribute('style', 'opacity: 0 !important')

      setTimeout(() => {
        white_dashed_circleUp.style.opacity = '0 !important'
      }, 1)
    }

    if (window.pageYOffset > 500 && select_lang) {
      wrap_first_line.setAttribute('id', 'menu_fixed')
      run_project.style.color = 'white'
      select_lang.style.display = 'none'
    }

    if (window.pageYOffset > 1000 && select_lang) {
      wrap_first_line.removeAttribute('id')
      wrap_first_line.setAttribute('id', 'menu_fixed_second')
      select_lang.style.display = ''
      run_project.style.color = ''
    }

    if (window.pageYOffset > 2100) {
      wrap_first_line.style.transform = 'translateY(-300px)'
    }

    setTimeout(() => {
      anim_logo()
    }, 1000)
  }

  /* Меню */
  function show_white_menu () {
    menu_wrapper.style.position = 'fixed'
    menu_wrapper.style.top = '0'
    menu_wrapper.style.left = '0'
    menu_wrapper.style.right = '0'
    menu_wrapper.style.zIndex = '10'
    const logo__link = menu_wrapper.querySelector('.logo__link')
    logo__link.classList.remove('menu__logoBlack')
    const run_project = menu_wrapper.querySelector('.run_project')
    run_project.classList.remove('color_red')
    run_project.classList.add('color_white')
    const menuOpen__hamb = document.querySelector('.menuOpen__hamb')
    menuOpen__hamb.innerHTML = ''
    menuOpen__hamb.setAttribute('id', 'whiteSandwich')
  }

  function show_regular_menu () {
    menu_wrapper.style.position = ''
    menu_wrapper.style.top = ''
    menu_wrapper.style.left = ''
    menu_wrapper.style.right = ''
    menu_wrapper.style.zIndex = ''

    const logo__link = menu_wrapper.querySelector('.logo__link')
    logo__link.classList.add('menu__logoBlack')

    const run_project = menu_wrapper.querySelector('.run_project')
    run_project.classList.remove('color_white')
    run_project.classList.add('color_red')

    const menuOpen__hamb = document.querySelector('.menuOpen__hamb')
    menuOpen__hamb.innerHTML = '<span>toggle menu</span>'
    menuOpen__hamb.removeAttribute('id')
  }

})

