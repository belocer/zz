<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Проекты</title>
    <link rel="stylesheet" href="css/main.css">

</head>
<body>
<div class="full-wrapper">
    <div class="contact__wrap_ticker_violet">
        <div class="ticker_violet">
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;
            <span class="ticker__text_violet">Наши проектьi&nbsp;</span>&nbsp;

        </div>
    </div>
    <?php include 'menuLight.php' ?>

    <div class="project__wrapper menu__off">
        <div id="projectTabs">
            <div class="projectTabs__title">
                <div class="projectTabs__item whiteborder" id="tab_1">
                    Наружня реклама
                    <img src="img/arrowRed.svg" alt="Наружня реклама">
                </div>
                <div class="projectTabs__item" id="tab_2">
                    Реклама на транспорте
                    <img src="img/arrowRed.svg" alt="Реклама на транспорте">
                </div>
                <div class="projectTabs__item" id="tab_3">
                    Реклама в метро
                    <img src="img/arrowRed.svg" alt="Реклама в метрое">
                </div>
                <div class="projectTabs__item" id="tab_4">
                    Промоутеры и промоакции
                    <img src="img/arrowRed.svg" alt="Промоутеры и промоакции">
                </div>
                <div class="projectTabs__item" id="tab_5">
                    Реклама в прессе
                    <img src="img/arrowRed.svg" alt="Реклама в прессе">
                </div>
                <div class="projectTabs__item" id="tab_6">
                    Indoor реклама
                    <img src="img/arrowRed.svg" alt="Indoor реклама">
                </div>
                <div class="projectTabs__item" id="tab_7">
                    Реклама на тв
                    <img src="img/arrowRed.svg" alt="Реклама на тв">
                </div>
                <div class="projectTabs__item" id="tab_8">
                    Реклама на радио
                    <img src="img/arrowRed.svg" alt="Реклама на радио">
                </div>
                <div class="projectTabs__item" id="tab_9">
                    Реклама в интернете
                    <img src="img/arrowRed.svg" alt="Реклама в интернете">
                </div>
                <div class="projectTabs__item" id="tab_10">
                    Реклама в лифтах
                    <img src="img/arrowRed.svg" alt="Реклама в лифтах">
                </div>
                <div class="projectTabs__item" id="tab_11">
                    Сувениры и полиграфия
                    <img src="img/arrowRed.svg" alt="Сувениры и полиграфия">
                </div>
            </div>
            <div class="projectTabs__content">
                <div class="projectTabsContent show">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
                <div class="projectTabsContent hide">
                    <div class="projectItem projectItem__1">
                        <img src="img/project__1.png" alt="" >
                        <p>Рекламный ролик для фирменной сети <br> пиццерий “Папа Джонс”</p>
                    </div>

                    <div class="projectItem projectItem__2">
                        <img src="img/project__2.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__3">
                        <img src="img/project__3.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__4">
                        <img src="img/project__4.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__5">
                        <img src="img/project__5.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__6">
                        <img src="img/project__6.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__7">
                        <img src="img/project__7.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__8">
                        <img src="img/project__8.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                    <div class="projectItem projectItem__9">
                        <img src="img/project__9.png" alt="" >
                        <p>Рекламный ролик для фирменной <br> сети пиццерий “Папа Джонс”</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include 'modalStartProject.php' ?>
<?php include 'menuOpen.php' ?>

<script src="js/main.min.js?ver=<?=rand(10000, 99999)?>"></script>

</body>
</html>