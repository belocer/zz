<div class="fm__wrapper">
    <div class="form__title">
        <h2>Или отправьте заявку сейчас</h2>
        <p>И мы сами свяжемся с вами!</p>
    </div>
</div>
<div class="form__white"></div>
<div class="fm__wrapper">
    <div id="tabs">
        <div class="tabs__title">
            <div class="tab__item whiteborder">
                Начать проект
            </div>
            <div class="tab__item">
                Задать вопрос
            </div>
            <div class="tab__item">
                Стать частью команды
            </div>
            <div class="tab__item">
                Стать партнёром
            </div>
        </div>
        <div class="tabs__content">
            <div class="tabContent show" >
                <form action="#" class="form" method="post" id="form__1">
                    <div class="fm__wrapper form__wrapper">
                        <div class="form__left">
                            <div class="form__input">
                                <input type="text"
                                       id="name"
                                       name="user_name"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Ваше имя"
                                >
                            </div>
                            <div class="form__input">
                                <input type="tel"
                                       id="phone"
                                       name="user_phone"
                                       autocomplete="off"
                                       placeholder="Ваш телефон"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                >
                            </div>
                        </div>
                        <div class="form__center">
                            <p>или</p>
                        </div>
                        <div class="form__right">
                            <div class="form__input">
                                <input type="text"
                                       id="company"
                                       name="user_company"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Название компании"
                                >
                            </div>
                            <div class="form__input">
                                <input type="email" placeholder="Контактный e-mail"
                                       id="email" name="user_email"
                                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                       oninvalid="this.setCustomValidity(' ')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form__all">
                            <div class="form__input">
                                <input type="text"
                                       id="task"
                                       name="user_task"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Опишите задачу"
                                >
                            </div>
                        </div>
                        <div class="file">
                            <input type="file" name="file"  class="file__input" id="file__1">
                            <label for="file__1" class="file__label" >
                                <img class="file__img" src="img/file.svg" alt="Прикрепите файл">
                                <span class="file__text">Прикрепите файл</span>
                            </label>
                        </div>
                        <div class="send">
                            <button class="form__btn" type="submit">
                                Отправить заявку <img src="img/send.svg" alt="отправить">
                            </button>
                            <p class="form__send">
                                Нажимая на кнопку “Отправить заявку”, вы даете согласие на
                                <a href="#" class="form__sendPolitics">обработку персональных данных</a> и соглашаетесь с
                                <a href="#" class="form__sendPolitics">политикой конфиденциальности</a>
                            </p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="tabContent hide">
                <form action="#" class="form" method="post">
                    <div class="fm__wrapper form__wrapper">
                        <div class="form__left">
                            <div class="form__input">
                                <input type="text"
                                       id="name"
                                       name="user_name"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Ваше имя11111"
                                >
                            </div>
                            <div class="form__input">
                                <input type="tel"
                                       id="phone2"
                                       name="user_phone"
                                       autocomplete="off"
                                       placeholder="Ваш телефон"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                >
                            </div>
                        </div>
                        <div class="form__center">
                            <p>или</p>
                        </div>
                        <div class="form__right">
                            <div class="form__input">
                                <input type="text"
                                       id="company"
                                       name="user_company"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Название компании"
                                >
                            </div>
                            <div class="form__input">
                                <input type="email"         placeholder="Контактный e-mail"
                                       id="email" name="user_email"
                                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                       oninvalid="this.setCustomValidity(' ')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form__all">
                            <div class="form__input">
                                <input type="text"
                                       id="task"
                                       name="user_task"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Опишите задачу"
                                >
                            </div>
                        </div>
                        <div class="file">
                            <input type="file" name="file" id="file__2" class="file__input">
                            <label for="file__2" class="file__label">
                                <img class="file__img" src="img/file.svg" alt="Прикрепите файл">
                                <span class="file__text">Прикрепите файл</span>
                            </label>
                        </div>
                        <div class="send">
                            <button class="form__btn">
                                Отправить заявку <img src="img/send.svg" alt="отправить"
                                                      type="submit"
                                >
                            </button>
                            <p class="form__send">
                                Нажимая на кнопку “Отправить заявку”, вы даете согласие на
                                <a href="#" class="form__sendPolitics">обработку персональных данных</a> и соглашаетесь с
                                <a href="#" class="form__sendPolitics">политикой конфиденциальности</a>
                            </p>
                        </div>
                    </div>

                </form>
            </div>
            <div class="tabContent hide">
                <form action="#" class="form" method="post">
                    <div class="fm__wrapper form__wrapper">
                        <div class="form__left">
                            <div class="form__input">
                                <input type="text"
                                       id="name"
                                       name="user_name"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Ваше имя"
                                >
                            </div>
                            <div class="form__input">
                                <input type="tel"
                                       id="phone3"
                                       name="user_phone"
                                       autocomplete="off"
                                       placeholder="Ваш телефон"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                >
                            </div>
                        </div>
                        <div class="form__center">
                            <p>или</p>
                        </div>
                        <div class="form__right">
                            <div class="form__input">
                                <input type="text"
                                       id="company"
                                       name="user_company"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Название компании"
                                >
                            </div>
                            <div class="form__input">
                                <input type="email"         placeholder="Контактный e-mail"
                                       id="email" name="user_email"
                                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                       oninvalid="this.setCustomValidity(' ')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form__all">
                            <div class="form__input">
                                <input type="text"
                                       id="task"
                                       name="user_task"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Опишите задачу"
                                >
                            </div>
                        </div>
                        <div class="file">
                            <input type="file" name="file" id="file__3" class="file__input">
                            <label for="file__3" class="file__label">
                                <img class="file__img" src="img/file.svg" alt="Прикрепите файл">
                                <span class="file__text">Прикрепите файл</span>
                            </label>
                        </div>
                        <div class="send">
                            <button class="form__btn">
                                Отправить заявку <img src="img/send.svg" alt="отправить"
                                                      type="submit"
                                >
                            </button>
                            <p class="form__send">
                                Нажимая на кнопку “Отправить заявку”, вы даете согласие на
                                <a href="#" class="form__sendPolitics">обработку персональных данных</a> и соглашаетесь с
                                <a href="#" class="form__sendPolitics">политикой конфиденциальности</a>
                            </p>
                        </div>
                    </div>

                </form>
            </div>
            <div class="tabContent hide">
                <form action="#" class="form" method="post">
                    <div class="fm__wrapper form__wrapper">
                        <div class="form__left">
                            <div class="form__input">
                                <input type="text"
                                       id="name"
                                       name="user_name"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Ваше имя"
                                >
                            </div>
                            <div class="form__input">
                                <input type="tel"
                                       id="phone4"
                                       name="user_phone"
                                       autocomplete="off"
                                       placeholder="Ваш телефон"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                >
                            </div>
                        </div>
                        <div class="form__center">
                            <p>или</p>
                        </div>
                        <div class="form__right">
                            <div class="form__input">
                                <input type="text"
                                       id="company"
                                       name="user_company"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Название компании"
                                >
                            </div>
                            <div class="form__input">
                                <input type="email"         placeholder="Контактный e-mail"
                                       id="email" name="user_email"
                                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$"
                                       oninvalid="this.setCustomValidity(' ')" oninput="setCustomValidity('')">
                            </div>
                        </div>
                        <div class="form__all">
                            <div class="form__input">
                                <input type="text"
                                       id="task"
                                       name="user_task"
                                       autocomplete="off"
                                       oninvalid="this.setCustomValidity(' ')"
                                       oninput="setCustomValidity('')"
                                       placeholder="Опишите задачу"
                                >
                            </div>
                        </div>
                        <div class="file">
                            <input type="file" name="file" id="file__4" class="file__input">
                            <label for="file__4" class="file__label">
                                <img class="file__img" src="img/file.svg" alt="Прикрепите файл">
                                <span class="file__text">Прикрепите файл</span>
                            </label>
                        </div>
                        <div class="send">
                            <button class="form__btn">
                                Отправить заявку <img src="img/send.svg" alt="отправить"
                                                      type="submit"
                                >
                            </button>
                            <p class="form__send">
                                Нажимая на кнопку “Отправить заявку”, вы даете согласие на
                                <a href="#" class="form__sendPolitics">обработку персональных данных</a> и соглашаетесь с
                                <a href="#" class="form__sendPolitics">политикой конфиденциальности</a>
                            </p>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>